const express = require('express');
const router = express.Router();

//pagina principal
router.get('/', async (req, res) => {
    res.render('index');
});

module.exports = router;