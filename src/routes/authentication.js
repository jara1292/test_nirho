const express = require('express');
const router = express.Router();

const passport = require('passport');
const { isLoggedIn } = require('../lib/auth');

// registrarse get vista
router.get('/signup', (req, res) => {
  res.render('auth/signup');
});
//registrarse post datos
router.post('/signup', passport.authenticate('local.signup', {
  successRedirect: '/news/news',
  failureRedirect: '/signup',
  failureFlash: true
}));

// inicar sesión vista get
router.get('/signin', (req, res) => {
  res.render('auth/signin');
});

//iniciar sesión post datos 
router.post('/signin', (req, res, next) => {
  req.check('username', 'Username es requerido').notEmpty();
  req.check('password', 'Password es requerido').notEmpty();
  const errors = req.validationErrors();
  if (errors.length > 0) {
    req.flash('message', errors[0].msg);
    res.redirect('/signin');
  }
  passport.authenticate('local.signin', {
    successRedirect: '/news/news',
    failureRedirect: '/signin',
    failureFlash: true
  })(req, res, next);
});


//salir de la aplicación
router.get('/logout', (req, res) => {
  req.logOut();
  res.redirect('/');
});

//vista perfil de usuario
router.get('/profile', isLoggedIn, (req, res) => {
  res.render('profile');
});

module.exports = router;
