const express = require('express');
const router = express.Router();

const pool = require('../database');
const { isLoggedIn } = require('../lib/auth');
const Request = require("request");

//Obtener noticias de api y retornarlas en vista
router.get('/news', (req, res) => {
    //res.render('news/listnews');
    var news = [];
    Request.get("https://newsapi.org/v2/everything?q=tesla&from=2021-02-28&sortBy=publishedAt&apiKey=f353137001304a479048c889425cf75e", (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        news = JSON.parse(body).articles;
        //console.log(news);
        res.render('news/listnews2',{data: news});
        //res.json(news);
    });
});


//prueba vista node
router.get('/', isLoggedIn, async (req, res) => {
    res.render('news/listnews');
});


module.exports = router;