--BASE DE DATOS EN MYSQL

CREATE DATABASE db_test_nirho;

USE db_test_nirho;

-- TABLE USER
CREATE TABLE users (
  id INT(11) NOT NULL,
  username VARCHAR(16) NOT NULL,
  password VARCHAR(60) NOT NULL,
  fullname VARCHAR(100) NOT NULL
);

ALTER TABLE users
  ADD PRIMARY KEY (id);

ALTER TABLE users
  MODIFY id INT(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;


INSERT INTO users (id, username, password, fullname) 
  VALUES (1, 'jara', '$2a$10$82G0KykmbC5T0VZtUyxUYuThIDEqM82wn0GOQVt1szLjF3i3zLjaC', 'Jose Antonio');

SELECT * FROM users;